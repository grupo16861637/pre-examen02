import express from "express";
export const router = express.Router();

export default {router};
//declarar primer ruta por omision

router.get('/',(req,res)=>{
    //parametros
    const params = {
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipo_servicio: req.query.tipo_servicio,
        kilowatts: req.query.kilowatts,
        subtotal:req.query.subtotal,
        impuesto: req.query.impuesto,
        descuento: req.query.descuento,
        totalpago: req.query.totalpago,
        costo:req.query.costo
    };
res.render('pagoluz',params);
});
    
    
router.post('/pagoluz',(req,res)=>{
    const params={
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipo_servicio: req.body.tipo_servicio,
        kilowatts: req.body.kilowatts,
        subtotal:req.body.subtotal,
        impuesto: req.body.impuesto,
        descuento: req.body.descuento,
        totalpago: req.body.totalpago,
        costo:req.body.costo
    }
    res.render('pagoluz',params);
});