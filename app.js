import express from 'express';

import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';
import misRutas from './router/index.js';


const puerto = 3005;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//declarar la variable punto de inicio

const main = express();

main.set("view engine","ejs");
main.use(express.static(path.join(__dirname, 'public')));
main.use(json.urlencoded({extended:true}));
main.use(misRutas.router);
//declarar primer ruta por omision

main.listen(puerto,()=>{
    console.log("se inicio el servidor por el puerto: http://localhost:"+puerto);
});
